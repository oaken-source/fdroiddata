Categories:Office
License:Apache2
Web Site:https://github.com/dmfs/tasks/blob/HEAD/README.md
Source Code:https://github.com/dmfs/tasks
Issue Tracker:https://github.com/dmfs/tasks/issues

Auto Name:Tasks
Summary:Keep track of your list of goals
Description:
A simple task manager app, allowing you to categorise your todo list by
urgency, state, timeframe etc.

Tasks can be synchronised with a CalDAV server using [[at.bitfire.davdroid]].

Status: Beta.
.

Repo Type:git
Repo:https://github.com/dmfs/tasks.git

Build:1.0.3,10
    commit=f9d88f8280
    srclibs=task-provider@4b402e7118
    extlibs=android/android-support-v4.jar
    prebuild=sed -i 's@\(reference.1=\).*@\1$$task-provider$$@' project.properties

Build:1.0.5,15
    commit=1.0.5
    init=rm -r releases
    srclibs=task-provider@c14e63ea9c
    extlibs=android/android-support-v4.jar
    prebuild=sed -i 's@\(reference.1=\).*@\1$$task-provider$$@' project.properties

Build:1.0.7-pre5,22
    commit=1.0.7
    srclibs=1:task-provider@ad70644d94749,2:PagerSlidingTabStrip@v1.0.1
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        mv libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs

Build:1.0.8,29
    commit=1.0.8
    srclibs=1:task-provider@ad70644d94749,2:PagerSlidingTabStrip@v1.0.1
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        mv libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs

Build:1.0.10,43
    disable=retention-magic-1.2.2 is not released
    commit=1.0.10
    srclibs=1:task-provider@1.0.10,2:PagerSlidingTabStrip@v1.0.1
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        mv libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs

Build:1.0.12,48
    commit=1.0.12
    srclibs=1:task-provider@1.0.12,2:PagerSlidingTabStrip@v1.0.1,RetentionMagic@v1.2.2
    rm=libs/android-retention-magic-*.jar
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        cp libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs && \
        cp -fR $$RetentionMagic$$/src/org src/

Build:1.1.1,76
    commit=1.1.1
    srclibs=1:task-provider@1.1.1,2:PagerSlidingTabStrip@v1.0.1,3:Support/v7/appcompat@android-5.0.1_r1,RetentionMagic@v1.2.2,DashClock@v1.2,xmlobjects@0.3
    rm=libs/android-retention-magic-*.jar
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        echo -e 'java.source=1.7\njava.target=1.7' | tee $$Support$$/ant.properties > ant.properties && \
        cp libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs && \
        cp libs/android-support-v4.jar $$Support$$/libs && \
        cp -fR $$RetentionMagic$$/src/org src/ && \
        cp -fR $$xmlobjects$$/src/org src/ && \
        cp -fR $$DashClock$$/api/src/com src/ && \
        sed -i -e '175d' src/org/dmfs/tasks/TaskGroupPagerAdapter.java
    target=android-21

Build:1.1.6,84
    commit=a9afea5a5f3e856fafc82753fe6ec974856f0e34
    srclibs=1:task-provider@1.1.1,2:PagerSlidingTabStrip@v1.0.1,3:Support/v7/appcompat@android-5.0.1_r1,RetentionMagic@v1.2.2,DashClock@v1.2,xmlobjects@0.3
    rm=libs/android-retention-magic-*.jar
    extlibs=android/android-support-v4.jar
    prebuild=mkdir -p $$PagerSlidingTabStrip$$/libs && \
        echo -e 'java.source=1.7\njava.target=1.7' | tee $$Support$$/ant.properties > ant.properties && \
        cp libs/android-support-v4.jar $$PagerSlidingTabStrip$$/libs && \
        cp libs/android-support-v4.jar $$Support$$/libs && \
        cp -fR $$RetentionMagic$$/src/org src/ && \
        cp -fR $$xmlobjects$$/src/org src/ && \
        cp -fR $$DashClock$$/api/src/com src/ && \
        sed -i -e '175d' src/org/dmfs/tasks/TaskGroupPagerAdapter.java
    target=android-21

Maintainer Notes:
Upstream forgot to tag/push --tags the last release. Switch back to UCM:Tags
and maybe AUM when appropriated.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.6
Current Version Code:84

