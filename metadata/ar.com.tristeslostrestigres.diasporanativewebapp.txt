Categories:Internet
License:GPLv3
Web Site:https://github.com/martinchodev/Diaspora-Native-WebApp/blob/HEAD/README.md
Source Code:https://github.com/martinchodev/Diaspora-Native-WebApp
Issue Tracker:https://github.com/martinchodev/Diaspora-Native-WebApp/issues
Changelog:https://trello.com/b/8z6gzo05/diaspora-native-webapp
Donate:http://martinv.tip.me

Auto Name:Diaspora Native WebApp
Summary:Diaspora client
Description:
Client for the Diaspora social network.
.

Repo Type:git
Repo:https://github.com/martinchodev/Diaspora-Native-WebApp

Build:1.0,2
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.1,3
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.2,4
    commit=v1.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:4

