Disabled:No license
Categories:Office
License:Unknown
Web Site:https://bitbucket.org/grennis/mongo-explorer
Source Code:https://bitbucket.org/grennis/mongo-explorer/src
Issue Tracker:https://bitbucket.org/grennis/mongo-explorer/issues

Auto Name:Mongo Explorer
Summary:Browse MongoDB databases
Description:
Mongo database client UI for Android! Featuring:

* Connect to MongoDB databases (MongoHQ, MongoLab, custom...)
* List, add, and remove collections
* Add, edit and delete documents
* Browse documents
* Write custom queries
.

Repo Type:git
Repo:https://bitbucket.org/grennis/mongo-explorer.git

Build:1.2.0,9
    disable=no license
    commit=61a1ffb
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.0
Current Version Code:9

